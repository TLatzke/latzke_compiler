#Program Name: Program1
.data 
number:             .word    0


.text
main:
addi   $s0,   $zero, 2
sw     $s0,   number

BeginWhile1:
lw     $t0,   number
addi   $t1,   $zero, 20
slt    $t0,   $t0,   $t1
beq    $t0,   $zero,  EndWhile1
lw     $t0,   number
addi   $t1,   $zero, 1
add    $s0,   $t0,   $t1
sw     $s0,   number

lw     $t0,   number
addi   $t1,   $zero, 2
div   $t0,   $t1
mfhi   $t0
addi   $t1,   $zero, 0
bne    $t0,   $t1,  Else1
lw     $a0,   number
addi   $v0,   $zero,   1
syscall
j      Exit1
Else1:
addi   $a0,   $zero, 0
addi   $v0,   $zero,   1
syscall
Exit1:

j      BeginWhile1
EndWhile1:
li     $v0,   10
syscall


#SubProgram Declarations
