#Program Name: Program2
.data 
newLine:            .asciiz  "\n"
upperLimit:         .word    0
prime:              .word    0
iterator:           .word    0
isPrime:            .word    0
divisor:            .word    0
temp1:              .word    0


.text
main:
addi   $s0,   $zero, 2
sw     $s0,   iterator

BeginWhile1:
lw     $t0,   iterator
addi   $t1,   $zero, 50
slt    $t0,   $t0,   $t1
beq    $t0,   $zero,  EndWhile1
lw     $a0,   iterator
jal    isPrimeNumber
add    $s0,   $v0,   $zero
sw     $s0,   prime

lw     $t0,   prime
beq    $t0,   $zero,  Else1
lw     $a0,   iterator
addi   $v0,   $zero,   1
syscall
li    $v0,   4
la    $a0,   newLine
syscall
j      Exit1
Else1:
addi   $s0,   $zero, 30
sw     $s0,   prime
Exit1:

lw     $t0,   iterator
addi   $t1,   $zero, 1
add    $s0,   $t0,   $t1
sw     $s0,   iterator
j      BeginWhile1
EndWhile1:
li     $v0,   10
syscall


#SubProgram Declarations
isPrimeNumber:
addi   $sp,   $sp,   -4
sw     $ra,   0($sp)
sw     $a0,   temp1
addi   $s0,   $zero, 1
sw     $s0,   isPrime
addi   $s0,   $zero, 2
sw     $s0,   divisor
lw     $t0,   temp1
addi   $t1,   $zero, 2
div    $t0,   $t1
mflo   $s0
sw     $s0,   upperLimit

BeginWhile2:
lw     $t0,   divisor
lw     $t1,   upperLimit
addi   $t1,   1
slt    $t0,   $t0,   $t1
beq    $t0,   $zero,  EndWhile2

lw     $t0,   temp1
lw     $t1,   divisor
div    $t0,   $t1
mfhi   $t0
addi   $t1,   $zero, 0
bne    $t0,   $t1,  Else2
addi   $s0,   $zero, 0
sw     $s0,   isPrime
j      Exit2
Else2:
lw     $s0,   isPrime
sw     $s0,   isPrime
Exit2:

lw     $t0,   divisor
addi   $t1,   $zero, 1
add    $s0,   $t0,   $t1
sw     $s0,   divisor
j      BeginWhile2
EndWhile2:
lw     $v0,   isPrime
lw     $ra,   0($sp)
addi   $sp,   $sp,   4
jr     $ra

