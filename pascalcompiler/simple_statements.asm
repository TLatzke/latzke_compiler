#Program Name: Program1
.data 
newLine:            .asciiz  "\n"
myid:               .word    0
factor:             .word    0
cmodel:             .word    0
number:             .word    0
dname:              .word    0


.text
main:
addi   $t0,   $zero, 4
addi   $t1,   $zero, 9
add    $s0,   $t0,   $t1
sw     $s0,   dname
lw     $s0,   dname
sw     $s0,   cmodel

lw     $t0,   number
lw     $t1,   cmodel
slt    $t0,   $t0,   $t1
beq    $t0,   $zero,  Else1
addi   $s0,   $zero, 5
sw     $s0,   number
j      Exit1
Else1:
addi   $s0,   $zero, 3
sw     $s0,   number
Exit1:


lw     $t0,   number
lw     $t1,   cmodel
slt    $t0,   $t1,   $t0
beq    $t0,   $zero,  Else2
addi   $s0,   $zero, 5
sw     $s0,   number
j      Exit2
Else2:
addi   $s0,   $zero, 3
sw     $s0,   number
Exit2:


lw     $t0,   number
lw     $t1,   cmodel
addi   $t1,   1
slt    $t0,   $t0,   $t1
beq    $t0,   $zero,  Else3
addi   $s0,   $zero, 5
sw     $s0,   number
j      Exit3
Else3:
addi   $s0,   $zero, 3
sw     $s0,   number
Exit3:


lw     $t0,   number
lw     $t1,   cmodel
addi   $t0,   1
slt    $t0,   $t1,   $t0
beq    $t0,   $zero,  Else4
addi   $s0,   $zero, 5
sw     $s0,   number
j      Exit4
Else4:
addi   $s0,   $zero, 3
sw     $s0,   number
Exit4:


lw     $t0,   number
lw     $t1,   cmodel
bne    $t0,   $t1,  Else5
addi   $s0,   $zero, 5
sw     $s0,   number
j      Exit5
Else5:
addi   $s0,   $zero, 3
sw     $s0,   number
Exit5:


lw     $t0,   number
lw     $t1,   cmodel
beq    $t0,   $t1,  Else6
addi   $s0,   $zero, 5
sw     $s0,   number
j      Exit6
Else6:
addi   $s0,   $zero, 3
sw     $s0,   number
Exit6:


lw     $t0,   number
beq    $t0,   $zero,  Else7
addi   $s0,   $zero, 5
sw     $s0,   number
j      Exit7
Else7:
addi   $s0,   $zero, 3
sw     $s0,   number
Exit7:

li     $v0,   10
syscall


#SubProgram Declarations
