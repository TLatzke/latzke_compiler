/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pascalcompiler;

/**
 *
 * @author Travis
 */
public class IdInformation {
    //class variable
    private String name;
    private String kind;
    private PascalToken type;
    private Integer length;
    
    //manipulator methods
    IdInformation() {
        this.name = null;
        this.kind = null;
        this.type = null;
        this.length = null;
    }
    public void setName(String newName) {
        this.name = newName;
    }
    
    public void setKind(String newKind) {
        this.kind = newKind;
    }
    
    public void setType(PascalToken newType) {
        this.type = newType;
    }
    
    public void setLength(int newLength) {
        this.length = newLength;
    }
    
    //variable retrieval methods
    public String getName() {return name;}
    public String getKind() {return kind;}
    public PascalToken getType() {return type;}
    public int getLength() {return length;}
    
    public void printId() {
        System.out.println("IdInformation: Name = "
                + this.name + "; Kind = "
                + this.kind + "; Type = "
                + this.type + "; Array Length = "
                + this.length);
    }
}
