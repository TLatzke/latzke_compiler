/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pascalcompiler;

import java.util.Hashtable;

/**
 *
 * @author Travis
 */
public class LookupTable extends Hashtable {
    
    public LookupTable () {
        super();
        this.put("+", PascalToken.PLUS);
        this.put("-", PascalToken.MINUS);
        this.put("*", PascalToken.MULTIPLY);
        this.put("/", PascalToken.DIVIDE);
        this.put("=", PascalToken.EQUALS);
        this.put("{", PascalToken.LEFT_BRACE);
        this.put("}", PascalToken.RIGHT_BRACE);
        this.put("(", PascalToken.LEFT_PARENTHESIS);
        this.put(")", PascalToken.RIGHT_PARENTHESIS);
        this.put("[", PascalToken.LEFT_BRACKET);
        this.put("]", PascalToken.RIGHT_BRACKET);
        this.put(";", PascalToken.SEMI_COLON);
        this.put(".", PascalToken.PERIOD);
        this.put(",", PascalToken.COMMA);
        this.put(":=", PascalToken.ASSIGNMENT);
        this.put(":", PascalToken.COLON);
        this.put("<", PascalToken.LESS_THAN);
        this.put(">", PascalToken.GREATER_THAN);
        this.put("<=", PascalToken.LESS_THAN_EQUAL);
        this.put(">=", PascalToken.GREATER_THAN_EQUAL);
        this.put("<>", PascalToken.NOT_EQUAL);
        this.put("var", PascalToken.VAR);
        this.put("array", PascalToken.ARRAY);
        this.put("integer", PascalToken.INTEGER);
        this.put("real", PascalToken.REAL);
        this.put("function", PascalToken.FUNCTION);
        this.put("procedure", PascalToken.PROCEDURE);
        this.put("begin", PascalToken.BEGIN);
        this.put("end", PascalToken.END);
        this.put("program", PascalToken.PROGRAM);
        this.put("if", PascalToken.IF);
        this.put("then", PascalToken.THEN);
        this.put("else", PascalToken.ELSE);
        this.put("while", PascalToken.WHILE);
        this.put("do", PascalToken.DO);
        this.put("or", PascalToken.OR);
        this.put("div", PascalToken.DIV);
        this.put("mod", PascalToken.MOD);
        this.put("and", PascalToken.AND);
        this.put("of", PascalToken.OF);
        this.put("not", PascalToken.NOT);

    }
}