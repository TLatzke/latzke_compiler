/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pascalcompiler;

import java.io.File;

/**
 *
 * @author Travis
 */
public class ParserTest {
    public static void main(String[] args) {
        File input = null;
        PascalParser pp = null;
        SymbolTable idTable = new SymbolTable();
        
        System.out.println("simple_program.txt");
        input = new File("test\\parser\\simple_program.txt");
        pp = new PascalParser(input, idTable);
        pp.program();
        idTable.printTable();
        System.out.println();
        
        idTable = new SymbolTable();
        System.out.println("subprogram_test.txt");
        input = new File("test\\parser\\subprogram_test.txt");
        pp = new PascalParser(input, idTable);
        pp.program();
        idTable.printTable();
        System.out.println();
        
        idTable = new SymbolTable();
        System.out.println("statement_test.txt");
        input = new File("test\\parser\\statement_test.txt");
        pp = new PascalParser(input, idTable);
        pp.program();
        idTable.printTable();
        System.out.println();
        
        idTable = new SymbolTable();
        System.out.println("invalid_parse.txt");
        input = new File("test\\parser\\invalid_parse.txt");
        pp = new PascalParser(input, idTable);
        pp.program();
        idTable.printTable();
    }
    
}
