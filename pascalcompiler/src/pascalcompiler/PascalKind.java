/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pascalcompiler;

/**
 *
 * @author Travis
 */
public class PascalKind {
    final public static String PROGRAM_NAME = "progam";
    final public static String VARIABLE_NAME = "variable";
    final public static String FUNCTION_NAME = "function";
    final public static String PROCEDURE_NAME = "procedure";
    final public static String ARRAY_NAME = "array";
    
    private String kind;
    public void setKind(String newKind) {
        this.kind = newKind;
    }
    public String getKind() {return this.kind;}
}
