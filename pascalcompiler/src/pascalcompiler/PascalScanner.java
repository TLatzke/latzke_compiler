package pascalcompiler;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PushbackReader;
import java.util.Hashtable;

/*
 * @author Travis Latzke
 * This class is used for scanning a pascal text file.
 * 
 * This class is used to create and provide PascalScanner objects with the
 * necessary attrributes and methods to properly scan a pascal file and return
 * pascal tokens to the pascal parser.
 */
public class PascalScanner {
    /*
     * The following constants represent the states in the DFA that recognizes
     * the pascal language. 
     */
    private static final int START_STATE = 0;
    private static final int IN_NUMBER_STATE = 1;
    private static final int IN_LESS_THAN_STATE = 2;
    private static final int IN_GREATER_THAN_STATE = 3;
    private static final int IN_COLON_STATE = 4;
    private static final int IN_LETTER_STATE = 5;
    private static final int ERROR_STATE = 50;
    private static final int SYMBOL_COMPLETE_STATE = 51;
    private static final int NUMBER_COMPLETE_STATE = 52;
    private static final int LESS_THAN_EQUAL_COMPLETE = 53;
    private static final int LESS_THAN_COMPLETE = 54;
    private static final int NOT_EQUAL_COMPLETE = 55;
    private static final int GREATER_THAN_EQUAL_COMPLETE = 56;
    private static final int GREATER_THAN_COMPLETE = 57;
    private static final int ASSIGNMENT_COMPLETE = 58;
    private static final int COLON_COMPLETE = 59;
    private static final int ID_COMPLETE = 60;
    
    /*
     * The following three values are used as possible return values 
     * in the nextToken() method. 
     */
    public static final int TOKEN_AVAILABLE = 0;
    public static final int TOKEN_NOT_AVAILABLE = 1;
    public static final int INPUT_COMPLETE = 2;

    private int[][] transitionTable;  // table[state][char]
    private PascalToken token;
    private Object attribute;
    private PushbackReader inputReader;
    private Hashtable lookupTable;

    /*
     * 
     * Initializes a PascalScanner object with an input file and 
     * a look up table.
     * 
     * This is a PascalScanner constructor that uses a specified look up table
     * to determine what keyword and symbol tokens are in the input file that
     * is given to this object.
     * 
     * @param File a file containing text to be scanned by the scanner
     * @param Hashtable a lookup table consisting of pascal keywords and symbols
     * 
     */
    public PascalScanner( File inputFile, Hashtable lookupTable) {
        this.lookupTable = lookupTable;
        try {
            this.inputReader = new PushbackReader( new FileReader(inputFile));
        }
        catch( Exception e) {
            e.printStackTrace();
            System.exit( 1);
        }
        this.transitionTable = createTransitionTable();
    }
    
    /*
     * Returns the attribute of the current token.
     * 
     * @return The attribute value of the current token
     */
    public Object getAttribute() { return( this.attribute);}
    
    /*
     * Finds the current token from the scanner.
     * 
     * @return  the last token scanned from the text file.
     */
    public PascalToken getToken() { return( this.token);}
    
    /*
     * Checks for next token.
     * 
     * This method is used to find the next available token in the input file.
     * 
     * @return returns TOKEN_AVAILABLE, TOKEN_NOT_AVAILABLE, or INPUT_COMPLETE
     * 
     */
    public int nextToken() {
        int currentState = START_STATE;
        StringBuilder lexeme = new StringBuilder("");
        while( currentState < ERROR_STATE ) {
            char currentChar = '\0';
            try {
                currentChar = (char) inputReader.read();
            }
            catch( Exception e) {
                // End of stream here.
                System.out.println("Should return input complete");
                return( INPUT_COMPLETE);
            }
            //System.out.println("current char is actually " + (int)currentChar);
            if( currentChar == 65535 && currentState == START_STATE) {
                return( INPUT_COMPLETE);
            }// end of file
            /*
             * If the end of the file has been reached, but the current state 
             * is not the start state, then the current state is in one of the
             * intermediate states. Since there is no more input, the current
             * state can switch to a final state.
             */
            else if( currentChar == 65535) {
                switch( currentState) {
                    case IN_NUMBER_STATE:
                        token = PascalToken.NUMBER;
                        attribute = lexeme.toString();
                        currentState = START_STATE;
                        break;
                        
                    case IN_LESS_THAN_STATE:
                        token = PascalToken.LESS_THAN;
                        attribute = lexeme.toString();
                        currentState = START_STATE;
                        break;
                        
                    case IN_GREATER_THAN_STATE:
                        token = PascalToken.GREATER_THAN;
                        attribute = lexeme.toString();
                        currentState = START_STATE;
                        break;
                        
                    case IN_COLON_STATE:
                        token = PascalToken.COLON;
                        attribute = lexeme.toString();
                        currentState = START_STATE;
                        break;
                        
                    case IN_LETTER_STATE:
                        if (lookupTable.containsKey(lexeme.toString())) {
                            token = (PascalToken)lookupTable.get( lexeme.toString());
                            attribute = lexeme.toString();
                        }
                        else {
                            token = PascalToken.ID;
                            attribute = lexeme.toString();
                        } 
                        break;
                }
                return( TOKEN_AVAILABLE);
            }// end of file
            
            if( currentChar > 127) return( TOKEN_NOT_AVAILABLE);
            //System.out.print("State:" + currentState + " Char: " + currentChar + " (" + (int)currentChar + ")");
            int nextState = transitionTable[currentState][currentChar];
            //System.out.println(" Next State: " + nextState + " current lexeme: ["+ lexeme.toString() + "]");
            switch( nextState) {
                // Start State
                case START_STATE:
                    lexeme = new StringBuilder("");
                    break;
                                    
                // find a number (an int at this point)
                case IN_NUMBER_STATE:
                    lexeme.append( currentChar);
                    break;
                    
                case IN_LESS_THAN_STATE:
                    lexeme.append( currentChar);
                    break;
                    
                case IN_GREATER_THAN_STATE:
                    lexeme.append( currentChar);
                    break;
                    
                case IN_COLON_STATE:
                    lexeme.append( currentChar);
                    break;
                    
                case IN_LETTER_STATE:
                    lexeme.append( currentChar);
                    break;
                    
                case NUMBER_COMPLETE_STATE:
                    try {
                        inputReader.unread(currentChar);
                    }
                    catch( IOException ioe) {}
                    token = PascalToken.NUMBER;
                    attribute = lexeme.toString();
                    break;
                    
                case SYMBOL_COMPLETE_STATE:
                    // After a single symbol, just push out the single lexeme
                    lexeme.append( currentChar);
                    token = (PascalToken)lookupTable.get( lexeme.toString());
                    attribute = lexeme.toString();
                    break;
                    
                case LESS_THAN_EQUAL_COMPLETE:
                    lexeme.append( currentChar);
                    token = PascalToken.LESS_THAN_EQUAL;
                    attribute = lexeme.toString();
                    break;
                    
               case GREATER_THAN_EQUAL_COMPLETE:
                   lexeme.append( currentChar);
                    token = PascalToken.GREATER_THAN_EQUAL;
                    attribute = lexeme.toString();
                    break;
                   
               case NOT_EQUAL_COMPLETE:
                   lexeme.append( currentChar);
                    token = PascalToken.NOT_EQUAL;
                    attribute = lexeme.toString();
                    break;
                   
               case LESS_THAN_COMPLETE:
                    try {
                        inputReader.unread(currentChar);
                    }
                    catch( IOException ioe) {}
                    token = PascalToken.LESS_THAN;
                    attribute = lexeme.toString();
                    break;
                   
               case GREATER_THAN_COMPLETE:
                    try {
                        inputReader.unread(currentChar);
                    }
                    catch( IOException ioe) {}
                    token = PascalToken.GREATER_THAN;
                    attribute = lexeme.toString();
                    break;
                   
               case ASSIGNMENT_COMPLETE:
                    lexeme.append( currentChar);
                    token = PascalToken.ASSIGNMENT;
                    attribute = lexeme.toString();
                    break;
                   
               case COLON_COMPLETE:
                    try {
                        inputReader.unread(currentChar);
                    }
                    catch( IOException ioe) {}
                    token = PascalToken.COLON;
                    attribute = lexeme.toString();
                    break;
                   
               case ID_COMPLETE:
                    try {
                        inputReader.unread(currentChar);
                    }
                    catch( IOException ioe) {}
                    if (lookupTable.containsKey(lexeme.toString())) {
                        token = (PascalToken)lookupTable.get( lexeme.toString());
                        attribute = lexeme.toString();
                    }
                    else {
                        token = PascalToken.ID;
                        attribute = lexeme.toString();
                    } 
                    break;
                    
                case ERROR_STATE:
                    lexeme.append( currentChar);
                    token = null;
                    attribute = lexeme.toString();
                    return( TOKEN_NOT_AVAILABLE);
            } // end switch
            currentState = nextState;
        } // end while currentState <= ERROR_STATE
        return( TOKEN_AVAILABLE);
    }

    /*
     * A transition table that represents the DFA that recognizes the pascal
     * language.
     * 
     * This transition table uses 2d array and is based off a DFA. The current
     * state of the DFA is used as the first index for the 2d array (i.e the row
     * of the table) and an ascii character representation is used as the second 
     * index for the 2d array. The value returned from the indexes is the state
     * that the DFA would be in after transitioning from its previous state 
     * based on what the next character was. 
     * 
     * @return A transition table representing a pascal language DFA
     */
    private int[][] createTransitionTable() {
        return new int[][]
    {
        // State 0: Start 
        {
           50,50,50,50,50,50,50,50,50, 0, 0,50,50, 0,50,50, //   0 -  15 (00-0f)
           50,50,50,50,50,50,50,50,50,50,50,50,50,50,50,50, //  16 -  31 (10-1f)
            0,50,50,50,50,50,50,50,51,51,51,51,51,51,51,51, //  32 -  47 (20-2f)
            1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 4,51, 2,51, 3,50, //  48 -  63 (30-3f)
           50, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, //  64 -  79 (40-4f)
            5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5,51,51,51,50,50, //  80 -  95 (50-5f)
           50, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, //  96 - 111 (60-6f)
            5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 6,50,51,50,50  // 112 - 127 (70-7f)
        },
        // State 1: In Number
        {
           52,52,52,52,52,52,52,52,52,52,52,52,52,52,52,52, //   0 -  15 (00-0f)
           52,52,52,52,52,52,52,52,52,52,52,52,52,52,52,52, //  16 -  31 (10-1f)
           52,52,52,52,52,52,52,52,52,52,52,52,52,52,52,52, //  32 -  47 (20-2f)
            1, 1, 1, 1, 1, 1, 1, 1, 1, 1,52,52,52,52,52,52, //  48 -  63 (30-3f)
           52,52,52,52,52,52,52,52,52,52,52,52,52,52,52,52, //  64 -  79 (40-4f)
           52,52,52,52,52,52,52,52,52,52,52,52,52,52,52,52, //  80 -  95 (50-5f)
           52,52,52,52,52,52,52,52,52,52,52,52,52,52,52,52, //  96 - 111 (60-6f)
           52,52,52,52,52,52,52,52,52,52,52,52,52,52,52,52  // 112 - 127 (70-7f)
        },
        // State 2: less_than_in state
        {
           54,54,54,54,54,54,54,54,54,54,54,54,54,54,54,54, //   0 -  15 (00-0f)
           54,54,54,54,54,54,54,54,54,54,54,54,54,54,54,54, //  16 -  31 (10-1f)
           54,54,54,54,54,54,54,54,54,54,54,54,54,54,54,54, //  32 -  47 (20-2f)
           54,54,54,54,54,54,54,54,54,54,54,54,54,53,55,54, //  48 -  63 (30-3f)
           54,54,54,54,54,54,54,54,54,54,54,54,54,54,54,54, //  64 -  79 (40-4f)
           54,54,54,54,54,54,54,54,54,54,54,54,54,54,54,54, //  80 -  95 (50-5f)
           54,54,54,54,54,54,54,54,54,54,54,54,54,54,54,54, //  96 - 111 (60-6f)
           54,54,54,54,54,54,54,54,54,54,54,54,54,54,54,54  // 112 - 127 (70-7f)
        },
        // State 3: greater_than_in state
        {
           57,57,57,57,57,57,57,57,57,57,57,57,57,57,57,57, //   0 -  15 (00-0f)
           57,57,57,57,57,57,57,57,57,57,57,57,57,57,57,57, //  16 -  31 (10-1f)
           57,57,57,57,57,57,57,57,57,57,57,57,57,57,57,57, //  32 -  47 (20-2f)
           57,57,57,57,57,57,57,57,57,57,57,57,57,56,57,57, //  48 -  63 (30-3f)
           57,57,57,57,57,57,57,57,57,57,57,57,57,57,57,57, //  64 -  79 (40-4f)
           57,57,57,57,57,57,57,57,57,57,57,57,57,57,57,57, //  80 -  95 (50-5f)
           57,57,57,57,57,57,57,57,57,57,57,57,57,57,57,57, //  96 - 111 (60-6f)
           57,57,57,57,57,57,57,57,57,57,57,57,57,57,57,57  // 112 - 127 (70-7f)
        },
        // State 4: colon_in state
        {
           59,59,59,59,59,59,59,59,59,59,59,59,59,59,59,59, //   0 -  15 (00-0f)
           59,59,59,59,59,59,59,59,59,59,59,59,59,59,59,59, //  16 -  31 (10-1f)
           59,59,59,59,59,59,59,59,59,59,59,59,59,59,59,59, //  32 -  47 (20-2f)
           59,59,59,59,59,59,59,59,59,59,59,59,59,58,59,59, //  48 -  63 (30-3f)
           59,59,59,59,59,59,59,59,59,59,59,59,59,59,59,59, //  64 -  79 (40-4f)
           59,59,59,59,59,59,59,59,59,59,59,59,59,59,59,59, //  80 -  95 (50-5f)
           59,59,59,59,59,59,59,59,59,59,59,59,59,59,59,59, //  96 - 111 (60-6f)
           59,59,59,59,59,59,59,59,59,59,59,59,59,59,59,59  // 112 - 127 (70-7f)
        },
        // State 5: ID_in state
        {
           60,60,60,60,60,60,60,60,60,60,60,60,60,60,60,60, //   0 -  15 (00-0f)
           60,60,60,60,60,60,60,60,60,60,60,60,60,60,60,60, //  16 -  31 (10-1f)
           60,60,60,60,60,60,60,60,60,60,60,60,60,60,60,60, //  32 -  47 (20-2f)
            5, 5, 5, 5, 5, 5, 5, 5, 5, 5,60,60,60,60,60,60, //  48 -  63 (30-3f)
           60, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, //  64 -  79 (40-4f)
            5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5,60,60,60,60, //  80 -  95 (50-5f)
           60, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, //  96 - 111 (60-6f)
            5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5,60,60,60,60,60  // 112 - 127 (70-7f)
        },
        // State 6: comment state
        {
            6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, //   0 -  15 (00-0f)
            6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, //  16 -  31 (10-1f)
            6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, //  32 -  47 (20-2f)
            6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, //  48 -  63 (30-3f)
            6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, //  64 -  79 (40-4f)
            6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, //  80 -  95 (50-5f)
            6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, //  96 - 111 (60-6f)
            6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 0, 6, 6  // 112 - 127 (70-7f)
        }
                
                
    };
    }
}