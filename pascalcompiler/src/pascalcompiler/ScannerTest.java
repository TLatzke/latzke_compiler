/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pascalcompiler;

import java.io.File;
import java.util.Hashtable;

/**
 *
 * @author Travis
 */

/*
 * The scannerTest class is used to test the pascal scanner. A scannerTest
 * object is passed a file to scan when the object is instantiated. The object
 * then scans the file for pascal tokens and prints the token type and attribute
 * for each token in the file.
 */
public class ScannerTest {
    
    ScannerTest(File input) {
        // TODO code application logic here
        Hashtable symbols = new LookupTable();
        PascalScanner es = new PascalScanner( input, symbols);
        while( es.nextToken() < PascalScanner.INPUT_COMPLETE) {
            //System.out.println("Foo is " + foo);
            PascalToken at = es.getToken();
            Object attr = es.getAttribute();
            System.out.println("Token: " + at + "   Attribute: [" + attr + "]");
        }
    }
    
    public static void main(String[] args) {
        File input = null;
        ScannerTest scanning = null;
        
        // conducting test 1 for the pascal scanner
        System.out.println("Test 1\n");   
        input = new File("test\\scanner\\test_1.txt");
        scanning = new ScannerTest(input);
        
        // conducting test 2 for the pascal scanner
        System.out.println("Test 2\n");
        input = new File("test\\scanner\\test_2.txt");
        scanning = new ScannerTest(input);
        
        // conducting test 3 for the pascal scanner
        System.out.println("Test 3\n");
        input = new File("test\\scanner\\test_3.txt");
        scanning = new ScannerTest(input);
        
    }
}
