/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pascalcompiler;

import java.util.Enumeration;
import java.util.Stack;
import java.util.Hashtable;

/**
 *
 * @author Travis
 */
public class SymbolTable {
    Hashtable<String, IdInformation> symbols = new Hashtable<String, IdInformation>();
    
    public void add(String IdName, String IdKind) {
        IdInformation newId = new IdInformation();
        newId.setName(IdName);
        newId.setKind(IdKind);
        this.symbols.put(IdName, newId);
        //check if name already exists, do nothing
        //create an id information object for id
        //put it into table
    }
    public IdInformation getIdInfo(String idName) {
        return symbols.get(idName);
    }
    
    public boolean containsId(String idName) {
        if (symbols.containsKey(idName)) {
            return true;
        }
        else 
            return false;
    }
    public void printTable() {
        IdInformation Id = new IdInformation();
        Enumeration e = symbols.elements();
        while(e.hasMoreElements()) {
            Id = (IdInformation)e.nextElement();
            Id.printId();
        }
    }
    
}
