/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pascalcompiler;

/**
 *
 * @author Travis
 */
public class SymbolTableTest {
    public static void main(String[] args) {
        boolean contain;
        IdInformation id = new IdInformation();
        SymbolTable myTable = new SymbolTable();
        myTable.add("foo", PascalKind.PROGRAM_NAME);
        myTable.add("money", PascalKind.VARIABLE_NAME);
        myTable.add("time", PascalKind.FUNCTION_NAME);
        myTable.add("my_array", PascalKind.ARRAY_NAME);
        myTable.printTable();  
        id = myTable.getIdInfo("time");
        id.printId();
        contain = myTable.containsId("foo");
        System.out.println(contain);
        contain = myTable.containsId("foot");
        System.out.println(contain);
        
    }
}
