
package pascalcompiler.codegenerator;

import java.io.*;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import pascalcompiler.parser.PascalParser;
import pascalcompiler.parser.SymbolTable;
import pascalcompiler.syntaxtree.ProgramNode;
import pascalcompiler.syntaxtree.SyntaxTreeNode;

/**
 *
 * @author Travis
 */
public class CodeGeneratorMain {
    /**
     * Main class file for when a user uses this compiler to translate Pascal
     * into MIPS.
     * @param args
     */
    public static void main(String[] args) {
        if (args.length == 0) {
            System.out.println("Need to name a Pascal file to be compiled");
            System.exit(0);
        } 
        else {
            String fileName = getName(args[0]);
            generateAssemblyCode(fileName);
        }    
    }
    /**
     * Returns the name of the file before the file extension. It also makes
     * sure that the file extension is .pascal.
     * @param in
     * @return
     */
    public static String getName(String in) {
        char[] fileIn = in.toCharArray();
        String name = "", extension = "";
        int i = 0;
        while ( !((int)fileIn[i] == 46) ) {
            name += fileIn[i];
            i++;
        }
        while ( i < in.length() ) {
            extension += fileIn[i];
            i++;
        }
        if ( !(extension.compareTo(".pascal") == 0) ) {
            System.out.println("File extension is not .pascal");
            System.out.println("Extension found: " + extension);
            System.exit(0);
        }
        return name;
    }
    
    /**
     * Generates MIPS code for the file name given to to the method.
     * @param fileName 
     */
    public static void generateAssemblyCode(String fileName) {
        SymbolTable idTable = new SymbolTable();
        File input;
        PascalParser pp = null;
        try {
            input = new File(fileName + ".pascal");
            pp = new PascalParser(input, idTable);
        }
        catch (Exception e) {
            System.out.println();
        }
        SyntaxTreeNode syntaxTree = pp.program();
        String output;
        CodeGenerator myCode = new CodeGenerator();
        FileWriter out = null;
        PrintWriter asmFile = null;
        try {
            out = new FileWriter(fileName + ".asm");
            asmFile = new PrintWriter(out); 
        }
        catch (IOException e) {
            System.out.println(e.getMessage());
        }
        if(pp.isParsable()) {
            idTable = pp.getIDTable();
            output = myCode.writeCodeForRoot((ProgramNode)syntaxTree,idTable);
            asmFile.write(output);
            asmFile.close();
        }
        else {
            System.out.println(fileName + " could not be parsed. " 
                    + "A syntax tree will not be printed");
        }
    }
    
}
