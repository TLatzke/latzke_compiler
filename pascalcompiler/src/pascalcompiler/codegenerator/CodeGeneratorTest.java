
package pascalcompiler.codegenerator;

import java.io.File;
import java.io.*;
import pascalcompiler.parser.PascalParser;
import pascalcompiler.parser.SymbolTable;
import pascalcompiler.syntaxtree.ProgramNode;
import pascalcompiler.syntaxtree.SyntaxTreeNode;

/**
 * This class is used to test the functionality of all the code generator 
 * methods that are implemented inside the CodeGenerator class.
 * @author Travis
 */
public class CodeGeneratorTest {
    /**
     * The main method outputs the results of the tested files.
     * @param args
     */
    public static void main (String args[]) {
        //testFile("simple_statements");
        //testFile("write_statements");
        //testFile("evenNumberTest");
        testFile("primeNumber");
    }
    
    /**
     * Static method that is used to outputs the test results of a specified 
     * file.
     * @param fileName
     */
    public static void testFile(String fileName) {
        SymbolTable idTable = new SymbolTable();
        File input = new File("src\\pascalcompiler\\test\\codegeneration\\" + fileName + ".txt");
        PascalParser pp = new PascalParser(input, idTable);
        SyntaxTreeNode syntaxTree = pp.program();
        String output;
        CodeGenerator myCode = new CodeGenerator();
        FileWriter out = null;
        PrintWriter asmFile = null;
        try {
            out = new FileWriter(fileName + ".asm");
            asmFile = new PrintWriter(out); 
        }
        catch (IOException e) {
            System.out.println(e.getMessage());
        }
        if(pp.isParsable()) {
            System.out.println("********************Assembly-Language********************");
            idTable = pp.getIDTable();
            output = myCode.writeCodeForRoot((ProgramNode)syntaxTree,idTable);
            asmFile.write(output);
            asmFile.close();
            System.out.println(output);
        }
        else {
            System.out.println(fileName + " could not be parsed. " 
                    + "A syntax tree will not be printed");
        }
    }
}
