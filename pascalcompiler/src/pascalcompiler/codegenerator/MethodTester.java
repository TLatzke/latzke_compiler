/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pascalcompiler.codegenerator;

import pascalcompiler.scanner.PascalToken;

/**
 *
 * @author Travis
 */
public class MethodTester {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        CodeGenerator cg = new CodeGenerator();
        if (cg.isRelationalOperator(PascalToken.GREATER_THAN)) {
            System.out.println("Method works correctly");
        }
    }
}
