
package pascalcompiler.parser;
import pascalcompiler.scanner.PascalToken;

/**
 * Class for storing information about identifiers.
 * The SymbolTable class use a hashtable to map objects of type String to
 * objects of type IdInformation.
 * @author Travis
 */
public class IdInformation {
    //class variable
    private String name;
    private PascalToken kind;
    private PascalToken type;
    private Integer length;
    private int paramaters;
    
    /**
     * Constructor.
     */
    IdInformation() {
        this.name = null;
        this.kind = null;
        this.type = null;
        this.length = null;
        this.paramaters = 0;
    }
    
    /**
     * Sets the number of parameters that is associated with a function 
     * identifier.
     * @param p 
     */
    public void setParameters(int p) {
        paramaters = p;
    }
    
    /**
     * Method that stores the identifier name into IdInformation.
     * @param newName 
     */
    public void setName(String newName) {
        this.name = newName;
    }
    
    /**
     * Sets the identifier kind, whether it is a function, variable, procedure,
     * or array.
     * @param newKind 
     */
    public void setKind(PascalToken newKind) {
        this.kind = newKind;
    }
    
    /**
     * Sets the identifier type.
     * @param newType 
     */
    public void setType(PascalToken newType) {
        this.type = newType;
    }
    
    /**
     * If the identifier is an array, this method sets the length of the array.
     * @param newLength 
     */
    public void setLength(int newLength) {
        this.length = newLength;
    }
    
    /**
     * Returns the identifier name.
     * @return String 
     */
    public String getName() {return name;}
    
    /**
     * Returns what the identifier kind is.
     * @return PascalToken
     */
    public PascalToken getKind() {return kind;}
    
    /**
     * Returns the type of the identifier.
     * @return PascalToken
     */
    public PascalToken getType() {return type;}
    
    /**
     * Returns the length of an array identifier.
     * @return int
     */
    public int getLength() {return length;}
    
    /**
     * Returns the number of parameters a function identifier has.
     * @return int
     */
    public int getParamaters() {return paramaters;}
    
    /**
     * Prints all the information in an IdInformation class.
     */
    public void printId() {
        System.out.println("IdInformation: Name = "
                + this.name + "; Kind = "
                + this.kind + "; Type = "
                + this.type + "; Array Length = "
                + this.length + "; "
                + "Number of Arguments = " + this.paramaters);
    }
}
