
package pascalcompiler.parser;

import java.io.File;
import pascalcompiler.syntaxtree.ProgramNode;
import java.util.LinkedList;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * This class is used as a test class to test all PascalParser class. The class
 * uses a static method, which is called several times to test different text
 * files that were written in Pascal.
 * @author Travis
 */
public class ParserTest {
    /**
     * Main method that uses TestFile function to test different Pascal 
     * programs.
     * @param args
     */
    public static void main(String[] args) {        
        //testFile("simple_program.txt");
        //testFile("subprogram_test.txt");
        //testFile("statement_test.txt");
        //testFile("invalid_parse.txt");
        testFile("equations.txt");
    }
    
    /**
     * Creates a parser that parses the file given.
     * @param fileName 
     */
    public static void testFile(String fileName) {
        SymbolTable idTable = new SymbolTable();
        File input = new File("src\\pascalcompiler\\test\\parser\\" + fileName);
        PascalParser pp;
        System.out.println("Testing " + fileName + " if it can be parsed");
        pp = new PascalParser(input, idTable);
        idTable = pp.getIDTable();
        idTable.printTable();
        System.out.println();
        
    }
}
