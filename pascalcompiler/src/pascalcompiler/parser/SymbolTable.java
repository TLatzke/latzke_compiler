
package pascalcompiler.parser;

import java.util.Enumeration;
import java.util.Stack;
import java.util.Hashtable;
import pascalcompiler.scanner.PascalToken;

/**
 * The SymbolTable class acts like a hashtable and is used as reference for
 * all the identifiers declared in the Pascal program.
 * @author Travis
 */
public class SymbolTable {
    Hashtable<String, IdInformation> symbols = new Hashtable<String, IdInformation>();
    
    /**
     * Adds a new element to the table.
     * @param IdName
     * @param IdKind  
     */
    public void add(String IdName, PascalToken IdKind) {
        IdInformation newId = new IdInformation();
        newId.setName(IdName);
        newId.setKind(IdKind);
        this.symbols.put(IdName, newId);
        //check if name already exists, do nothing
        //create an id information object for id
        //put it into table
    }
    
    /**
     * Adds a new element to the table.
     * @param IdName
     * @param IdKind
     * @param type  
     */
    public void add(String IdName, PascalToken IdKind, PascalToken type) {
        IdInformation newId = new IdInformation();
        newId.setName(IdName);
        newId.setKind(IdKind);
        newId.setType(type);
        this.symbols.put(IdName, newId);
        //check if name already exists, do nothing
        //create an id information object for id
        //put it into table
    }
    
    /**
     * Sets the type of a specified identidfier.
     * @param varName
     * @param varType
     */
    public void setType(String varName, PascalToken varType) {
        this.getIdInfo(varName).setType(varType);
    }
    
    /**
     * Retruns a hashtable.
     * @return Hashtable<String, IdInformation>
     */
    public Hashtable<String, IdInformation> getTable() {return symbols;}
    
    /**
     * Returns an IdInformation object that is associated with the specified
     * idName.
     * @param idName
     * @return IdInformation
     */
    public IdInformation getIdInfo(String idName) {
        return symbols.get(idName);
    }
    
    /**
     * Returns true if the table already contains the specified id.
     * @param idName
     * @return boolean
     */
    public boolean containsId(String idName) {
        if (symbols.containsKey(idName)) {
            return true;
        }
        else 
            return false;
    }
    
    /**
     * Prints a table of all the id's and their information.
     */
    public void printTable() {
        IdInformation Id = new IdInformation();
        Enumeration e = symbols.elements();
        System.out.println("******************************Printing Symbol Table"
                + "******************************");
        while(e.hasMoreElements()) {
            Id = (IdInformation)e.nextElement();
            Id.printId();
        }
    }
    
}
