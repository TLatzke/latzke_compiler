
package pascalcompiler.parser;

import pascalcompiler.scanner.PascalToken;

/**
 * Tests SymbolTable class.
 * This class is used to test if the methods in SymbolTable are working 
 * correctly.
 * @author Travis
 */
public class SymbolTableTest {
    /**
     * Tests the methods in the SymbolTable class.
     * @param args
     */
    public static void main(String[] args) {
        boolean contain;
        IdInformation id = new IdInformation();
        SymbolTable myTable = new SymbolTable();
        myTable.add("foo", PascalToken.PROGRAM);
        myTable.add("money", PascalToken.VAR);
        myTable.add("time", PascalToken.FUNCTION);
        myTable.add("my_array", PascalToken.ARRAY);
        myTable.printTable();  
        id = myTable.getIdInfo("time");
        id.printId();
        contain = myTable.containsId("foo");
        System.out.println(contain);
        contain = myTable.containsId("foot");
        System.out.println(contain);
        
    }
}
