/*
 * PascalToken.java
 *
 */
package pascalcompiler.scanner;

/**
 *
 * @author Travis
 */
public enum PascalToken {
    /** number token*/
    NUMBER,
    /** plus token */
    PLUS,
    /** minus token*/
    MINUS,
    /** multiply token*/
    MULTIPLY,
    /** divide token*/
    DIVIDE,
    /** equals token*/
    EQUALS,
    /** left brace token*/
    LEFT_BRACE, 
    /** right brace token*/
    RIGHT_BRACE, 
    /** left parenthesis token*/
    LEFT_PARENTHESIS, 
    /** right parenthesis token*/
    RIGHT_PARENTHESIS, 
    /** left bracket token*/
    LEFT_BRACKET, 
    /** right bracket token*/
    RIGHT_BRACKET, 
    /** semi colon token*/
    SEMI_COLON,
    /** period token*/
    PERIOD,
    /** comma token*/
    COMMA,
    /** assignment token*/
    ASSIGNMENT,
    /** colon token*/
    COLON,
    /** less than token*/
    LESS_THAN,
    /** greater than token*/
    GREATER_THAN,
    /** less than equal token*/
    LESS_THAN_EQUAL,
    /** greater than equal token*/
    GREATER_THAN_EQUAL,
    /** not equal token*/
    NOT_EQUAL,
    /** id token*/
    ID,
    
    // Keywords
    
    /** variable token*/
    VAR,
    /** array token*/
    ARRAY,
    /** integer token*/
    INTEGER,
    /** real number token*/
    REAL,
    /** function token*/
    FUNCTION,
    /** procedure token*/
    PROCEDURE,
    /** begin token*/
    BEGIN,
    /** end token*/
    END,
    /** program token*/
    PROGRAM,
    /** if token*/
    IF,
    /** then token*/
    THEN,
    /** else token*/
    ELSE,
    /** while token*/
    WHILE,
    /** do token*/
    DO,
    /** or token*/
    OR,
    /** divide token*/
    DIV,
    /** modular arithmetic token token*/
    MOD,
    /** and token*/
    AND,
    /** of token*/
    OF,
    /** not token*/
    NOT,
    /** write token*/
    WRITE
}
