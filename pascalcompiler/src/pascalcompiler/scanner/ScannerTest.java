/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pascalcompiler.scanner;

import java.io.File;
import java.util.Hashtable;

/**
 *
 * @author Travis
 */

/**
 * The scannerTest class is used to test the pascal scanner. A scannerTest
 * object is passed a file to scan when the object is instantiated. The object
 * then scans the file for pascal tokens and prints the token type and attribute
 * for each token in the file.
 */
public class ScannerTest {
    
    ScannerTest(String fileName) {
        // TODO code application logic here
        String filePath;
        filePath = "src\\pascalcompiler\\test\\scanner\\" + fileName;
        File input = new File(filePath);
        Hashtable symbols = new LookupTable();
        PascalScanner es = new PascalScanner( input, symbols);
        while( es.nextToken() < PascalScanner.INPUT_COMPLETE) {
            //System.out.println("Foo is " + foo);
            PascalToken at = es.getToken();
            Object attr = es.getAttribute();
            System.out.println("Token: " + at + "   Attribute: [" + attr + "]");
        }
    }
    
    /**
     * Tests the Pascal scanner with several different files.
     * @param args
     */
    public static void main(String[] args) {
        ScannerTest scanTest;
        // conducting test 1 for the pascal scanner
        scanTest = new ScannerTest("test_1.txt");
        // conducting test 2 for the pascal scanner
        scanTest = new ScannerTest("test_2.txt");
        // conducting test 3 for the pascal scanner
        scanTest = new ScannerTest("test_3.txt");
        
    }
}
