
package pascalcompiler.syntaxtree;

import java.util.ArrayList;
import java.util.Iterator;
import pascalcompiler.scanner.PascalToken;

/**
 * Syntax tree node that holds all the information about a functions or 
 * procedures arguments.
 * @author Travis
 */
public class ArgumentsNode extends SyntaxTreeNode {
    private ArrayList<VariableNode> args;
    
    /**
     * Constructor.
     */
    public ArgumentsNode() {
        args = new ArrayList<VariableNode>();
    }
    
    /**
     * Adds an argument to the current list of arguments.
     * @param node
     */
    public void addVariable(VariableNode node) {args.add(node);}
    
    /**
     * Adds all the argument nodes from one list of arguments to another list.
     * @param newArgs
     */
    public void addArguments(ArgumentsNode newArgs) {
        this.args.addAll(newArgs.getArguments());
    }
    
    /**
     * Turns a list of strings into an argument list.
     * @param idList 
     * @param type 
     */
    public void setDeclarations(ArrayList<String> idList, PascalToken type) {
        Iterator<String> i = idList.iterator();
            VariableNode vNode;
            while (i.hasNext()) {
                vNode = new VariableNode((String)i.next(),type);
                args.add(vNode);
            }
    }
    
    /**
     * Returns an array list of the arguments.
     * @return 
     */
    public ArrayList<VariableNode> getArguments() {return args;}
    
    /**
     * Returns the number of arguments a function has.
     * @return 
     */
    public int getArgsNumber() {
        int argNumber = 0;
        Iterator i = args.iterator();
        while (i.hasNext()) {
            i.next();
            argNumber++;
        }
        return argNumber;
    }
    
    /**
     * Constructs a String that represents the syntax tree for this node.
     * @param level 
     * @return 
     */
    @Override
    public String indentedToString( int level) {
        String answer = super.indentedToString(level);   
        if (args != null) {
            answer += "Arguments" + "\n";
        }
        else {
            answer += "No arguments \n";
        }
        Iterator i = args.iterator();
        while (i.hasNext()) {
            answer += ((VariableNode)i.next()).indentedToString(level + 1);
        }
        return( answer);
    }
}
