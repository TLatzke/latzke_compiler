
package pascalcompiler.syntaxtree;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Creates a node in the syntax tree that contains a block of statements.
 * @author Travis
 */
public class CompoundStatementNode extends StatementNode {
    private ArrayList<StatementNode> sList;
    
    /**
     * Constructor.
     */
    public CompoundStatementNode() {
        sList = new ArrayList<StatementNode>();
    }
    
    /**
     * Returns a list of statement nodes that this node points to.
     * @return 
     */
    public ArrayList<StatementNode> getStatements() {
        return sList;
    }
    
    /**
     * Adds all the statements contained in one compound statement node to 
     * another compound statement node.
     * @param statements
     * @return 
     */
    public CompoundStatementNode addAll(CompoundStatementNode statements) {
        ArrayList<StatementNode> oldSList = new ArrayList<StatementNode>();
        oldSList.addAll(statements.getStatements());
        this.sList.addAll(oldSList);
        return this;
    }
    
    /**
     * Adds a statement node to a compound statement node.
     * @param node
     */
    public void addStatement(StatementNode node) {sList.add(node);}
    
    /**
     * Returns the statement where the function is assigned a value.
     * Returns null if there is no such statement.
     * @param functionName
     * @return 
     */
    public VarAssignStatement getReturnStatement(String functionName) {
        StatementNode tempStatement;
        Iterator<StatementNode> i = sList.iterator();
        while (i.hasNext()) {
            tempStatement = i.next();
            if (tempStatement instanceof VarAssignStatement){
                if ( ((VarAssignStatement)tempStatement).getVarName().equals(functionName)) {
                    return (VarAssignStatement)tempStatement;
                }
            }
        }
        return null;
    }
    
    /**
     * Constructs a String that represents the syntax tree for this node.
     * @param level 
     * @return 
     */
    @Override
    public String indentedToString( int level) {
        String answer = super.indentedToString(level);
        answer += "Compound Statement: ";
        if (sList.isEmpty()) {
            answer += "Empty \n";
        }
        else {
            answer += "\n";
        }
        Iterator<StatementNode> sIterator = sList.iterator();
        while (sIterator.hasNext()) {
            answer += sIterator.next().indentedToString(level + 1);
        }
        return( answer);
    }
    
}
