
package pascalcompiler.syntaxtree;
import java.util.ArrayList;
import java.util.Iterator;
import pascalcompiler.scanner.PascalToken;

/**
 * The DeclarationNode class is a node that makes up the syntax tree for a 
 * Pascal program.
 * @author Travis
 */
public class DeclarationsNode extends SyntaxTreeNode {
    ArrayList<VariableNode> variables;
    
    /**
     * Constructor.
     */
    public DeclarationsNode() {
        variables = new ArrayList<VariableNode>();
    }
    
    /**
     * Adds a variable to the declarations list.
     * @param node
     */
    public void addVariable(VariableNode node) {variables.add(node);}
    
    /**
     * Sets the declarations based upon a list of identifier strings.
     * @param idList
     * @param type
     */
    public void setDeclarations(ArrayList<String> idList, PascalToken type) {
        Iterator<String> i = idList.iterator();
            VariableNode vNode;
            while (i.hasNext()) {
                vNode = new VariableNode((String)i.next(),type);
                variables.add(vNode);
            }
    }
    
    /**
     * Returns all declared variables.
     * @return 
     */
    public ArrayList<VariableNode> getDeclarations() {
        return variables;
    }
    
    /**
     * Constructs a String that represents the syntax tree for this node.
     * @param level 
     * @return 
     */
    @Override
    public String indentedToString( int level) {
        String answer = super.indentedToString(level);
        answer += "Declarations: ";   
        if (variables.isEmpty()) {
            answer += "Empty \n";
        }
        else {
            answer += "\n";
        }
        Iterator i = variables.iterator();
        while (i.hasNext()) {
            answer += ((VariableNode)i.next()).indentedToString(level + 1);
        }
        return( answer);
    }
}
