
package pascalcompiler.syntaxtree;

/**
 * EquationNode is an abstract class that gives inheritance to several sub-
 * classes.
 * @author Travis
 */
public abstract class EquationNode extends SyntaxTreeNode {
    /**
     * This method converts this node to a string.
     * @return 
     */
    @Override
    public String toString() {
        return "EquationNode88\n";
    }
}
