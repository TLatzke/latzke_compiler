
package pascalcompiler.syntaxtree;

/**
 * The FunctionDeclarationNode is part of the syntax tree for a Pascal
 * program. It contains all the information about a function definition.
 * @author Travis
 */
public class FunctionDeclarationNode extends SubProgramNode {
    private String functionName;
    private String type;
    
    /**
     * Sets the functions name.
     * @param name
     */
    public FunctionDeclarationNode(String name) {
        this.functionName = name;
    }
    
    /**
     * Sets the return type of the function.
     * @param type
     */
    public void setType(String type) {
        this.type = type;
    }
    
    /**
     * Returns the functions name.
     * @return 
     */
    public String getName() {return functionName;}
    
    /**
     * Constructs a String that represents the syntax tree for this node.
     * @param level 
     * @return 
     */
    @Override
    public String indentedToString( int level) {
        String answer = super.indentedToString(level);
        answer += "Function: " + this.functionName + "\n";
        if (this.getArguments() != null) {
            answer += getArguments().indentedToString(level+1);
        }
        else {
            answer += super.indentedToString(level+1) + "No arguments\n";
        }
        answer += getDeclarations().indentedToString(level+1);
        answer += getSubPrograms().indentedToString(level+1);
        answer += getCompoundStatement().indentedToString(level+1);
        return( answer);
    }
}
