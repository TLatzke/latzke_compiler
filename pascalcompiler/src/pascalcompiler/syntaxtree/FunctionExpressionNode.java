
package pascalcompiler.syntaxtree;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * This node is part of the syntax tree for a Pascal program. It contains all 
 * the information for when a function is called during the execution of a 
 * program.
 * @author Travis
 */
public class FunctionExpressionNode extends EquationNode{
    private String name;
    private ArrayList<EquationNode> parameters;
    
    /**
     * Initializes the function name.
     * @param fname
     */
    public FunctionExpressionNode(String fname) {
        name = fname;
    }
    
    /**
     * Sets the parameters of the function.
     * @param list
     */
    public void setParameters(ArrayList<EquationNode> list) {parameters = list;}
    
    /**
     * Returns the name of the function.
     * @return function name
     */
    public String getName() {return name;}
    
    /**
     * Returns the parameters of the function.
     * @return list of parameters
     */
    public ArrayList<EquationNode> getParameters() {return parameters;}
    
    /**
     * Returns the number of arguments given to the function.
     * @return number of arguments
     */
    public int getArgsNumber() {
        int answer = 0;
        Iterator<EquationNode> i = parameters.iterator();
        while (i.hasNext()) {
            answer++;
            i.next();
        }
        return answer;
    }
    
    /**
     * Constructs a String that represents the syntax tree for this node.
     * @param level 
     * @return 
     */
    @Override
    public String indentedToString( int level) {
        String answer = super.indentedToString(level);
        answer += "Function Call: \n";
        answer += super.indentedToString(level+1);
        answer += this.name + "\n";
        Iterator<EquationNode> i = parameters.iterator();
        while (i.hasNext()) {
            answer += i.next().indentedToString(level+2);
        }
        return( answer);
    }
}
