
package pascalcompiler.syntaxtree;

/**
 *
 * @author Travis
 */
public class FunctionNode extends SubProgramNode {
    private String functionName;
    private String type;
    
    public FunctionNode(String name) {
        this.functionName = name;
    }

    
    public void setType(String type) {
        this.type = type;
    }
    
    public String getName() {return this.functionName;}
    
    @Override
    public String indentedToString( int level) {
        String answer = super.indentedToString(level);
        answer += "Function: " + this.functionName + "\n";
        answer += getArguments().indentedToString(level+1);
        answer += getDeclarations().indentedToString(level+1);
        answer += getSubPrograms().indentedToString(level+1);
        answer += getCompoundStatement().indentedToString(level+1);
        return( answer);
    }
}
