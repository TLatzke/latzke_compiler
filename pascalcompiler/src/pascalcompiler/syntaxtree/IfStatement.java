
package pascalcompiler.syntaxtree;

import java.util.Iterator;
import pascalcompiler.scanner.PascalToken;

/**
 * The ifStatement class is part of the Pascal syntax tree and contains all
 * of the information needed to execute an if statement.
 * @author Travis
 */
public class IfStatement extends StatementNode {
    private EquationNode ifExpression;
    private StatementNode thenStatement;
    private StatementNode elseStatement;
    
    /**
     * Constructs the nodes that an if node points at.
     */
    public IfStatement() {
        ifExpression = new EquationNode(){};
        thenStatement = new StatementNode() {};
        elseStatement = new StatementNode() {};
    }
    
    /**
     * Sets the expression for an if statement.
     * @param exp
     */
    public void setExpression(EquationNode exp) {
        ifExpression = exp;
    } 
    
    /**
     * Sets the statement for default statement of an if statement.
     * @param statement
     */
    public void setThenStatement(StatementNode statement) {
        thenStatement = statement;
    } 
    
    /**
     * Sets the statement for the else part of an if statement.
     * @param statement 
     */
    public void setElseStatement(StatementNode statement) {
        elseStatement = statement;
    }
    
    /**
     * Returns the expression for an if statement.
     * @return 
     */
    public EquationNode getExpression() {return ifExpression;}
    
    /**
     * Returns the default statement for an if statement.
     * @return default statement of an if statement
     */
    public StatementNode getThenStatement() {return thenStatement;}
    
    /**
     * Returns the else statement for an if statement.
     * @return 
     */
    public StatementNode getElseStatement() {return elseStatement;}
    
    /**
     * Converts the node to a string.
     * @return 
     */
    @Override
    public String toString() {
        return "If Statement\n";
    }
    
    /**
     * Constructs a String that represents the syntax tree for this node.
     * @param level 
     * @return 
     */
    @Override
    public String indentedToString( int level) {
        String answer = super.indentedToString(level);
        answer += "If Statement: \n";
        answer += super.indentedToString(level+1);
        answer += "If Expression:\n";
        answer += this.ifExpression.indentedToString(level+2);
        answer += super.indentedToString(level+1);
        answer += "Then Statement:\n";
        answer += this.thenStatement.indentedToString(level+2);
        answer += super.indentedToString(level+1);
        answer += "Else Statement:\n";
        answer += this.elseStatement.indentedToString(level+2);
        return( answer);
    }
}
