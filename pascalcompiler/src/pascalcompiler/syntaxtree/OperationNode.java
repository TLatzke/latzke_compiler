
package pascalcompiler.syntaxtree;
import pascalcompiler.scanner.PascalToken;

/**
 * OperationNode is part of the syntax tree structure for a Pascal program. An
 * OperationNode represents a mathematical equation as a tree structure.
 * @author Travis
 */
public class OperationNode extends EquationNode{
    /** The left operator of this operation. */
    private EquationNode left;
    
    /** The right operator of this operation. */
    private EquationNode right;
    
    /** The kind of operation. */
    private PascalToken operation;
    
    /**
     * Creates an operation node given an operation token.
     * @param op The token representing this node's math operation.
     */
    public OperationNode ( PascalToken op) {
        this.operation = op;
    }
    
    /**
     * Returns the left side of this expression.
     * @return
     */
    public EquationNode getLeft() { return( this.left);}
    
    /**
     * Returns the right side of this expression.
     * @return
     */
    public EquationNode getRight() { return( this.right);}
    
    /**
     * Returns the operation for this expression.
     * @return
     */
    public PascalToken getOperation() { return( this.operation);}
    
    /**
     * Sets the left side of an operation, which is an expression.
     * @param node
     */
    public void setLeft( EquationNode node) { this.left = node;}
    
    /**
     * Set the right side of an operation, which is an expression.
     * @param node
     */
    public void setRight( EquationNode node) { this.right = node;}
    
    /**
     * Sets the operation of the expression.
     * @param op
     */
    public void setOperation( PascalToken op) { this.operation = op;}
    
    /**
     * Returns true if the operation node has a right child and a left child.
     * @return 
     */
    public boolean hasBothNodes(){
        if (left != null && right != null && operation != null) {
            return true;
        }
        else 
            return false;
    }
    
    /**
     * Returns the operation token as a String.
     * @return The String version of the operation token.
     */
    @Override
    public String toString() {
        return operation.toString();
    }
    
    /**
     * Constructs a String that represents the syntax tree for this node.
     * @param level 
     * @return 
     */
    @Override
    public String indentedToString( int level) {
        String answer = super.indentedToString(level);
        answer += "Operation: " + this.operation + "\n";
        if (this.hasBothNodes()) {
            answer += left.indentedToString(level + 1);
            answer += right.indentedToString(level + 1);
        }
        return( answer);
    }
}
