
package pascalcompiler.syntaxtree;

/**
 * The ProcedureDeclarationNode class contains all the information about a
 * procedure declaration.
 * @author Travis
 */
public class ProcedureDeclarationNode extends SubProgramNode {
    private String procedureName;

    /** 
     * Initializes the procedures name.
     * @param name
     */
    public ProcedureDeclarationNode(String name) {
        this.procedureName = name;
    }

    /**
     * Returns the name of the procedure.
     * @return 
     */
    public String getName() {return this.procedureName;}
    
    /**
     * Constructs a String that represents the syntax tree for this node.
     * @param level 
     * @return 
     */
    @Override
    public String indentedToString( int level) {
        String answer = super.indentedToString(level);
        answer += "Procedure: " + this.procedureName + "\n";
        answer += getArguments().indentedToString(level+1);
        answer += getDeclarations().indentedToString(level+1);
        answer += getSubPrograms().indentedToString(level+1);
        answer += getCompoundStatement().indentedToString(level+1);
        return( answer);
    } 
}
