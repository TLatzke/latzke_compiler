/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pascalcompiler.syntaxtree;

/**
 *
 * @author Travis
 */
public class ProcedureNode extends SubProgramNode {
    private String procedureName;

    
    public ProcedureNode(String name) {
        this.procedureName = name;
    }

    public String getName() {return this.procedureName;}
    
    @Override
    public String indentedToString( int level) {
        String answer = super.indentedToString(level);
        answer += "Procedure: " + this.procedureName + "\n";
        answer += getArguments().indentedToString(level+1);
        answer += getDeclarations().indentedToString(level+1);
        answer += getSubPrograms().indentedToString(level+1);
        answer += getCompoundStatement().indentedToString(level+1);
        return( answer);
    } 
}
