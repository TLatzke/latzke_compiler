
package pascalcompiler.syntaxtree;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * The ProcedureStatementNode is part of the syntax tree for a Pascal program 
 * and is constructed based on when a statement calls a procedure.
 * @author Travis
 */
public class ProcedureStatement extends StatementNode{
    private ArrayList<EquationNode> expressions;
    private String procedureID;
    
    /**
     * Constructor.
     */
    public ProcedureStatement(){
        expressions = new ArrayList<EquationNode>(){};
    }
    
    /**
     * Sets the expressions that are arguments to the procedure.
     * @param exp
     */
    public void setExpressions(ArrayList<EquationNode> exp) {
        expressions = exp;
    }
    
    /**
     * Sets the name of the procedure.
     * @param idName
     */
    public void setID(String idName) {
        procedureID = idName;
    }
    
    /**
     * Returns the list of expressions that were passed as arguments to the 
     * procedure.
     * @return 
     */
    public ArrayList<EquationNode> getExpressions() {return expressions;}
    
    /**
     * Returns the name of the procedure.
     * @return
     */
    public String getName() {return procedureID;}
    
    /**
     * Returns the number of arguments given to the procedure.
     * @return 
     */
    public int getArgsNumber() {
        int answer = 0;
        Iterator i = expressions.iterator();
        while (i.hasNext()) {
            answer++;
            i.next();
        }
        return answer;
    }
      
    /**
     * Converts this node to a string.
     * @return 
     */
    @Override
    public String toString() {
        return "Procedure Statement\n";
    }
    
    /**
     * Constructs a String that represents the syntax tree for this node.
     * @param level 
     * @return 
     */
    @Override
    public String indentedToString( int level) {
        String answer = super.indentedToString(level);
        answer += "Procedure Statement: \n";
        answer += super.indentedToString(level+1);
        answer += procedureID + "\n";
        Iterator<EquationNode> i = expressions.iterator();
        while (i.hasNext()) {
            answer += i.next().indentedToString(level+2);
        }
        return( answer);
    }
    
}
