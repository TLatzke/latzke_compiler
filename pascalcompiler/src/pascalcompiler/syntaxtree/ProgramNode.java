
package pascalcompiler.syntaxtree;

/**
 * The program node is the root node for the entire syntax tree that is made
 * for a particular Pascal program. 
 * @author Travis
 */
public class ProgramNode extends SyntaxTreeNode {
    DeclarationsNode variables;
    CompoundStatementNode main;
    SubProgramDeclarationsNode subPrograms;
    String programName;
    
    /**
     * Initializes the name of the program.
     * @param name name of the program
     */
    public ProgramNode(String name){
        variables = null;
        main = null;
        subPrograms = null;
        programName = name;
    }
    
    /**
     * Returns a list of variables that were declared in the program.
     * @return
     */
    public DeclarationsNode getDeclarations() { return( this.variables);}
    
    /**
     * Returns the block of statements that make up the main part of the 
     * program.
     * @return 
     */
    public CompoundStatementNode getCompoundStatement() { return( this.main);}
    
    /**
     * Returns all of the information needed to define all the sub programs
     * declared.
     * @return
     */
    public SubProgramDeclarationsNode getSubDeclarations() { return( this.subPrograms);}
    
    /**
     * Returns the name of the program.
     * @return
     */
    public String getName() { return (this.programName); }
    
    /**
     * Sets the declarations of a pascal program.
     * @param node
     */
    public void setDeclarations( DeclarationsNode node) { this.variables = node;}
    
    /**
     * Builds the syntax tree that is associated with the CompoundStatementNode
     * class.
     * @param node
     */
    public void setCompoundStatement( CompoundStatementNode node) { this.main = node;}
    
    /**
     * Builds the node involved with defining the sub programs for a pascal
     * program.
     * @param node
     */
    public void setSubDeclarations( SubProgramDeclarationsNode node) { this.subPrograms = node;}
    
    /**
     * Constructs a String that represents the syntax tree for this node.
     * @param level 
     * @return 
     */
    @Override
    public String indentedToString( int level) {
        String answer = super.indentedToString(level);
        answer += "Program name: " + this.programName + "\n";
        answer += variables.indentedToString(level + 1);
        answer += main.indentedToString(level + 1);
        answer += subPrograms.indentedToString(level + 1);
        return( answer);
    }
}
