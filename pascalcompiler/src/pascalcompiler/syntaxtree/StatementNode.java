
package pascalcompiler.syntaxtree;

/**
 * Abstract class that represents all the different statements that could be
 * found inside a Pascal program.
 * @author Travis
 */
public abstract class StatementNode extends SyntaxTreeNode {
    
    /**
     * Converts this string to a string.
     * @return 
     */
    public String toString(){
        return "Statement\n";
    }
}
