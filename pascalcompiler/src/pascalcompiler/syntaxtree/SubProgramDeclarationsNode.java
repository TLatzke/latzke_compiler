
package pascalcompiler.syntaxtree;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Contains a list of all the sub programs that were defined in a Pascal source 
 * file.
 * @author Travis
 */
public class SubProgramDeclarationsNode extends SyntaxTreeNode {
    private ArrayList<SubProgramNode> subPrograms;
    
    /**
     * Constructor.
     */
    public SubProgramDeclarationsNode() {
        subPrograms = new ArrayList<SubProgramNode>();
    }
    
    /**
     * Adds a sub program object to the list of sub programs.
     * @param newSubProgram
     */
    public void addSubProgram(SubProgramNode newSubProgram) {
        subPrograms.add(newSubProgram);
    }
    
    /**
     * Returns a list of sub programs.
     * @return ArrayList<SubProgramNode>
     */
    public ArrayList<SubProgramNode> getSubPrograms() {
        return this.subPrograms;
    }
    
    /**
     * Constructs a String that represents the syntax tree for this node.
     * @param level 
     * @return 
     */
    @Override
    public String indentedToString( int level) {
        String answer = super.indentedToString(level);
        answer += "Sub-Program Declarations: ";
        if (subPrograms.isEmpty()) {
            answer += "Empty \n";
        }
        else {
            answer += "\n";
        }
        Iterator<SubProgramNode> i = subPrograms.iterator();
        while (i.hasNext()) {
            SubProgramNode sbd = i.next();
            if ( !(sbd == null) ) {
                answer += sbd.indentedToString(level + 1);
            }   
        }
        return( answer);
    }   
}
