
package pascalcompiler.syntaxtree;

import java.util.ArrayList;
import pascalcompiler.scanner.PascalToken;

/**
 * Holds the information for a sub program declaration. Functions and procedures
 * are the two types of sub programs that inherent from this class.
 * @author Travis
 */
public abstract class SubProgramNode extends SyntaxTreeNode {
    private String name;
    private PascalToken kind;
    private DeclarationsNode declarations;
    private SubProgramDeclarationsNode subPrograms;
    private CompoundStatementNode compoundStatement;
    private ArgumentsNode arguments;
    
    /**
     * Constructs the nodes of the syntax tree that represent all of the 
     * declared variables inside a sub program.
     * @param node
     */
    public void setDeclarations(DeclarationsNode node) { 
        declarations = node;
    }
    
    /**
     * Constructs a list of sub programs that is used by 
     * SubProgramDeclarationsNode.
     * @param node
     */
    public void setSubProgramDeclarations(SubProgramDeclarationsNode node) {
        subPrograms = node;
    }
    
    /**
     * Constructs the body of the function or procedure.
     * @param node
     */
    public void setCompoundStatement(CompoundStatementNode node) {
        compoundStatement = node;
    }
    
    /**
     * Sets the arguments that are specified in the sub program definition.
     * @param node
     */
    public void setArguments(ArgumentsNode node) {
        arguments = node;
    }
    
    /**
     * Sets the kind of sub program that is defined, whether it is a procedure
     * or a function.
     * @param kind
     */
    public void setKind(PascalToken kind) {
        this.kind = kind;
    }
    
    /**
     * Returns the syntax tree dealing with declared variables inside a 
     * sub program.
     * @return
     */
    public DeclarationsNode getDeclarations(){return declarations;}
    
    /**
     * Returns the syntax tree dealing with all the declared sub programs.
     * @return
     */
    public SubProgramDeclarationsNode getSubPrograms(){return subPrograms;}
    
    /**
     * Returns the syntax tree representing the body of the function.
     * @return
     */
    public CompoundStatementNode getCompoundStatement(){return compoundStatement;}
    
    /**
     * Returns the syntax tree that represents the sub programs defined 
     * arguments.
     * @return 
     */
    public ArgumentsNode getArguments() {return arguments;}
    
    /**
     * Returns the number of arguments defined in a sub program.
     * @return
     */
    public int getArgsNumber() {return arguments.getArgsNumber();}
    
    /**
     * Returns a PascalToken.FUNCTION or PascalToken.PROCEDURE depending on 
     * what kind of sub program this object is.
     * @return
     */
    public PascalToken getKind() {return kind;}
    
    /**
     * Converts this node to a String.
     * @return
     */
    @Override
    public String toString() {
        return "SubProgramNode";    
    }
}
