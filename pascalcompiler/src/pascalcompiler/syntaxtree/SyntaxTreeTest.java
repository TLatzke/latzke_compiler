
package pascalcompiler.syntaxtree;

import java.io.File;
import static pascalcompiler.parser.ParserTest.testFile;
import pascalcompiler.parser.PascalParser;
import pascalcompiler.parser.SymbolTable;

/**
 *
 * @author Travis
 */
public class SyntaxTreeTest {
    /**
     * Tests several Pascal files.
     * @param args
     */
    public static void main(String[] args) {
        //testFile("simple_program.txt");
        //testFile("subprogram_test.txt");
        //testFile("statement_test.txt");
        //testFile("equations.txt");
        testFile("primeNumber.txt");
    }
    /**
     * Method used to test a specified file.
     * @param fileName
     */
    public static void testFile(String fileName) {
        SymbolTable idTable = new SymbolTable();
        File input = new File("src\\pascalcompiler\\test\\syntaxtree\\" + fileName);
        PascalParser pp = new PascalParser(input, idTable);
        String syntaxTree = pp.program().indentedToString(0);
        if(pp.isParsable()) {
            System.out.println("Printing syntax tree for " + fileName);
            System.out.println("********************Syntax-Tree********************");
            System.out.println(syntaxTree);
            pp.getIDTable().printTable();
        }
        else {
            System.out.println(fileName + " could not be parsed. " 
                    + "A syntax tree will not be printed");
        }
        System.out.println();
        
    }
}
