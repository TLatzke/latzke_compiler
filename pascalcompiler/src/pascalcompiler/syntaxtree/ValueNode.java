
package pascalcompiler.syntaxtree;

/**
 *
 * @author Travis
 */
public class ValueNode extends EquationNode {
    /** The attribute associated with this node. */
    String attribute;
    boolean isID;
    
    /**
     * Creates a ValueNode with the given attribute.
     * @param attr The attribute for this value node.
     * @param kind
     */
    public ValueNode( String attr, String kind) {
        this.attribute = attr;
        if (kind == "id") 
            isID = true;
        else
            isID = false;
    }
    
    /**
     * Returns true if the value node is an id.
     * @return 
     */
    public boolean isID() {return isID;}
    
    /** 
     * Returns the attribute of this node.
     * @return The attribute of this ValueNode.
     */
    public String getAttribute() { return( this.attribute);}
    
    /**
     * Returns the attribute as the description of this node.
     * @return The attribute String of this node.
     */
    @Override
    public String toString() {
        return( attribute);
    }
    
    /**
     * Constructs a String that represents the syntax tree for this node.
     * @param level 
     * @return 
     */
    @Override
    public String indentedToString( int level) {
        String answer = super.indentedToString(level);
        answer += "Value: " + this.attribute + "\n";
        return answer;
    }
}
