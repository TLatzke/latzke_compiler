
package pascalcompiler.syntaxtree;
import pascalcompiler.parser.SymbolTable;
import pascalcompiler.scanner.PascalToken;

/**
 *
 * @author Travis
 */
public class VarAssignStatement extends StatementNode {
    private VariableNode vNode;
    private EquationNode expression;
    private String name;
    
    /**
     * Initializes the variable name.
     * @param id
     */
    public VarAssignStatement(String id) {
        vNode = new VariableNode(id);
        name = id;
        expression = new EquationNode() {};
    }
    
    /**
     * Sets the expression.
     * @param expression
     */
    public void setExpression(EquationNode expression) {
        this.expression = expression;
    }
    
    /**
     * Returns the expression node of this object.
     * @return
     */
    public EquationNode getExpression() { return expression;}
    
    /**
     * Returns the name of the variable
     * @return
     */
    public String getVarName() {return name;}
    
    /**
     * Returns true if the name of the identifier is a variable.
     * @param table
     * @return
     */
    public boolean isVariable(SymbolTable table) {
        if (table.getIdInfo(this.name).getKind() == PascalToken.VAR) {
            return true;
        }
        else {
            return false;
        }
    }
    
    /**
     * returns true if the name of the identifier is a function name.
     * @param table
     * @return
     */
    public boolean isFunction(SymbolTable table) {
        if (table.getIdInfo(this.name).getKind() == PascalToken.FUNCTION) {
            return true;
        }
        else {
            return false;
        }
    }
    
    /**
     * Converts this node to a string.
     * @return 
     */
    @Override
    public String toString() {
        return "Variable Assignment\n";
    }
    
    /**
     * Constructs a String that represents the syntax tree for this node.
     * @param level 
     * @return 
     */
    @Override
    public String indentedToString( int level) {
        String answer = super.indentedToString(level);
        answer += "Variable Assignment \":=\" \n";
        answer += this.vNode.indentedToString(level+1);
        if (this.expression != null) {
            answer += this.expression.indentedToString(level+1);
        }
        return( answer);
    }
}
