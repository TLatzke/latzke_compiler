
package pascalcompiler.syntaxtree;

import pascalcompiler.scanner.PascalToken;

/**
 *
 * @author Travis
 */
public class VariableNode extends SyntaxTreeNode{
    private String name;
    private PascalToken type;
    
    /**
     * Initializes the name of the variable.
     * @param name
     */
    public VariableNode(String name) {
        this.name = name;
    }
    
    /**
     * Initializes the variable name and type.
     * @param name
     * @param type
     */
    public VariableNode(String name, PascalToken type) {
        this.name = name;
        this.type = type;
    }

    VariableNode() {
    }
    
    /**
     * Sets the variable type.
     * @param type
     */
    public void setType(PascalToken type) {
        this.type = type;
    }
    
    /**
     * Returns the variable type.
     * @return
     */
    public PascalToken getType() {return type;}

    /**
     * Returns the name of the variable.
     * @return
     */
    public String getName() {return this.name;}
    
    /**
     * Constructs a String that represents the syntax tree for this node.
     * @param level 
     * @return 
     */
    @Override
    public String indentedToString( int level) {
        String answer = super.indentedToString(level);
        answer += "Variable: " + this.name + " ";
        if (this.type == null) {
            answer += "\n";
        }
        else {
            answer += this.type.toString() + "\n";
        }
        return( answer);
    }
}
