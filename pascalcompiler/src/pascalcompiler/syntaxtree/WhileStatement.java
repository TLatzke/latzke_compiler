
package pascalcompiler.syntaxtree;

/**
 *
 * @author Travis
 */
public class WhileStatement extends StatementNode {
    EquationNode expression;
    StatementNode statement;
    
    /**
     * Constructor.
     */
    public WhileStatement(){
        expression = new EquationNode(){};
        statement = new StatementNode() {};
    }
    
    /**
     * Returns the expression that is part of the while statement.
     * @return
     */
    public EquationNode getExpression() {return this.expression;}
    
    /**
     * Returns the statement node that is part of the while statement.
     * @return
     */
    public StatementNode getStatement() {return this.statement;}
    
    /**
     * 
     * @param expression
     */
    public void setExpression(EquationNode expression) {
        this.expression = expression;
    }
    
    /**
     *
     * @param statement
     */
    public void setStatementNode(StatementNode statement) {
        this.statement = statement;
    }
    
    /*
     * Converts this node to a string.
     * @return 
     */
    @Override
    public String toString() {
        return "While Statement\n";
    }
    
    /**
     * Constructs a String that represents the syntax tree for this node.
     * @param level 
     * @return 
     */
    @Override
    public String indentedToString( int level) {
        String answer = super.indentedToString(level);
        answer += "While Statement: \n";
        answer += super.indentedToString(level+1);
        answer += "While Expression:\n";
        answer += this.expression.indentedToString(level+2);
        answer += super.indentedToString(level+1);
        answer += "Do Statement:\n";
        answer += this.statement.indentedToString(level+2);
        return( answer);
    }
}
