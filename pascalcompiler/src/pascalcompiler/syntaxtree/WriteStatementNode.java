
package pascalcompiler.syntaxtree;

/**
 * This class is part of the syntax tree class and contains all the information
 * needed be passed along to the code generation process, so that assembly code 
 * can be written and make a system call to produce an output stream.
 * @author Travis
 */
public class WriteStatementNode extends StatementNode{
    private EquationNode expression;
    
    /**
     * Constructor.
     */
    public WriteStatementNode() {
        expression = new EquationNode() {};
    }
    
    /**
     *
     * @param exp
     */
    public void setExpression(EquationNode exp) {
        expression = exp;
    }
    
    /**
     *
     * @return
     */
    public EquationNode getExpression() {
        return this.expression;
    }
    
    /**
     * Constructs a String that represents the syntax tree for this node.
     * @param level 
     * @return 
     */
    @Override
    public String indentedToString( int level) {
        String answer = super.indentedToString(level);
        answer += "Write Statement: \n";
        answer += this.expression.indentedToString(level+1);
        return( answer);
    }
    
}
