#Program Name: Program1
.data 
newLine:            .asciiz  "\n"
number:             .word    0
dname:              .word    0
g:                  .word    0
f:                  .word    0
e:                  .word    0
d:                  .word    0
factor:             .word    0
cmodel:             .word    0
myid:               .word    0


.text
main:
addi   $t0,   $zero, 56
addi   $t1,   $zero, 10
div    $t0,   $t1
mflo   $a0
addi   $v0,   $zero,   1
syscall
li    $v0,   4
la    $a0,   newLine
syscall
addi   $t0,   $zero, 10
addi   $t1,   $zero, 7
div    $t0,   $t1
mfhi   $a0
addi   $v0,   $zero,   1
syscall
li    $v0,   4
la    $a0,   newLine
syscall
addi   $t0,   $zero, 3
addi   $t1,   $zero, 7
mult   $t0,   $t1
mflo   $a0
addi   $v0,   $zero,   1
syscall
li    $v0,   4
la    $a0,   newLine
syscall
li     $v0,   10
syscall


#SubProgram Declarations
foo:
addi   $sp,   $sp,   -4
sw     $ra,   0($sp)
sw     $a0,   d
sw     $a1,   e
sw     $a2,   f
sw     $a3,   g
lw     $t2,   d
lw     $t3,   e
add    $t1,   $t2,   $t3
lw     $t4,   f
add    $t0,   $t1,   $t4
lw     $t5,   g
add    $v0,   $t0,   $t5
lw     $ra,   0($sp)
addi   $sp,   $sp,   4
jr     $ra

